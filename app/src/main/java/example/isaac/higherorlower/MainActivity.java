package example.isaac.higherorlower;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }

    public void generateRandomNum () {
        Random random = new Random();
        int low = 1;
        int high = 20;
        result = random.nextInt(high-low) + low;
    }

    public void guessClick(View view) {
        EditText guessEditText = (EditText) findViewById(R.id.et_one);
        String guessString = guessEditText.getText().toString();
        int guessNumber = Integer.parseInt(guessString);

        if (guessNumber == result) {
            Toast.makeText(this, "You guessed the number!", Toast.LENGTH_LONG).show();
            generateRandomNum();
        } else if (guessNumber > result){
            Toast.makeText(this, "Lower!", Toast.LENGTH_LONG).show();
        } else if (guessNumber < result) {
            Toast.makeText(this, "Higher!", Toast.LENGTH_LONG).show();
        }
    }
}
